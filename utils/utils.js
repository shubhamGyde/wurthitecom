var month = new Array();
month[0] = "Jan";
month[1] = "Feb";
month[2] = "Mar";
month[3] = "Apr";
month[4] = "May";
month[5] = "Jun";
month[6] = "Jul";
month[7] = "Aug";
month[8] = "Sep";
month[9] = "Oct";
month[10] = "Nov";
month[11] = "Dec";

var utils = {
    getMonthName: function(dt){
        return month[dt.getMonth()];
    },
    getLastSixMonthNameYear: function(dt){
        var monthName = [];
        var monthNum = [];
        var year = [];
        var monthsDetails = {};
        for(var i = 6; i > 0; i -= 1) {
            d = new Date(dt.getFullYear(), dt.getMonth() - i, 1);
            monthNum.push(d.getMonth());
            monthName.push(month[d.getMonth()]);
            year.push(d.getFullYear());
        }
        monthsDetails.monthNums = monthNum;
        monthsDetails.monthNames = monthName;
        monthsDetails.years = year;

        return monthsDetails;
    }
};

module.exports = utils;