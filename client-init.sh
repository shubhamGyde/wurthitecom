#!/bin/sh
displayClientName=$1
dialogFlowToken=$2
devDialogFlowToken=$3
adminEmail=$4
adminPwd=$5
newClientName=`echo $displayClientName | tr " " "-" | tr "[:upper:]" "[:lower:]"`
jwtKey="Dai$newClientName"
sed -i "s/clientName/$newClientName/g" views/clientName.ejs
sed -i "s/paramClientName/$newClientName/g" clientName-support/webint/chatWidget.js
sed -i "s/paramclientName/$newClientName/g" clientName-support/clientName-base.html
sed -i "s/Client Name/$displayClientName/g" clientName-support/clientName-base.html
mv clientName-support/clientName-base.html "clientName-support/$newClientName-base.html"
mv clientName-support "$newClientName-support"
sed -i "s/DaiClientName/$jwtKey/g" config/config.js
sed -i "s/clientName@123/$adminPwd/g" config/config.js
sed -i "s/clientName/$newClientName/g" config/config.js
sed -i "s/ceo@clientname.com/$adminEmail/g" config/config.js
sed -i "s/57eb639662c34a9295493c20cbefc78f/$devDialogFlowToken/g" config/config.js
sed -i "s/6a5ff3ccf54249deb8077a2463f1e2b2/$dialogFlowToken/g" server.js
sed -i "s/var clientName = \"clientName\";/var clientName = \"$newClientName\";/g" server.js
sed -i "s/var clientDisplayName = \"Client Name\";/var clientDisplayName = \"$displayClientName\";/g" server.js
sed -i "s/changeClientNameHere~~!234/$newClientName/g" build/static/js/main.c40270b0.js
sed -i "s/changeClientNameHere~~!234/$newClientName/g" build/static/js/main.c40270b0.js.map
rm -rf .git/