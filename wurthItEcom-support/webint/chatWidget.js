var clientName = "wurthItEcom";
var link = document.createElement( "link" );
// var clientUrl = 'http://localhost:3003/';
//var clientUrl = 'http://10.110.31.39:3003/';

var clientUrl = 'https://wurthbot.wurth-it.in/';

link.href = clientUrl+'webint/webStyle.css';
link.type = "text/css";
link.rel = "stylesheet";
link.media = "screen,print";
document.getElementsByTagName( "body" )[0].appendChild( link );
var baseUrl = window.location.href;

if(!window.jQuery)
{
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = clientUrl+'scripts/js-files/jquery.min.js';
    script.async = 'false';
    document.getElementsByTagName('head')[0].appendChild(script);
}

var containerDiv = document.createElement('div');
containerDiv.id = 'daiContainerDiv';
containerDiv.className = 'daiContainer';

var chatIconDiv = document.createElement('div');
chatIconDiv.id = 'daichaticon';
chatIconDiv.className = 'chat-icon';


var chatIconImg = document.createElement('input');
chatIconImg.id = 'daichat-btn';
chatIconImg.type = 'image';
chatIconImg.src = clientUrl+'chatIcon.png';
chatIconImg.className = 'chat-icon-img';

var modalDiv = document.createElement('div');
modalDiv.id = 'myModalDai';
modalDiv.className = 'modal';
modalDiv.setAttribute('role','dialog');
modalDiv.style="position:relative !important";

var modalChildDiv = document.createElement('div');
modalChildDiv.id = 'myModalChildDai';
modalChildDiv.className = 'lab-modal-body modal-sm frame';
modalChildDiv.style= 'border: 1px solid #D3D3D3;';

var modalPopup = document.createElement('div');
modalPopup.id = 'modalPopup';
modalPopup.className = 'lab-modal-body-popup frame floating';
modalPopup.style= 'border: 1px solid #D3D3D3; margin-bottom:30px; height:auto;width: auto;max-width: 285px;';

var iframe = document.createElement('iframe');
var pathname = window.location.pathname; // Returns path only

var url = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');

iframe.src = clientUrl+''+clientName+'-base.html?baseUrl='+baseUrl;
iframe.id = 'web-iframe';
iframe.height = '100%';
iframe.width = '100%';
iframe.style = 'border: 1px; z-index: 300000; overflow: scroll; -webkit-overflow-scrolling: touch;';

chatIconDiv.appendChild(chatIconImg);
modalChildDiv.appendChild(iframe);
modalDiv.appendChild(modalChildDiv);
containerDiv.appendChild(chatIconDiv);
containerDiv.appendChild(modalPopup);
containerDiv.appendChild(modalDiv);
document.getElementsByTagName('body')[0].appendChild(containerDiv);



var vdoObj = document.getElementsByClassName('herohome');
if(vdoObj.length > 0)
    vdoObj[0].style = 'z-index:1000';



    var createCookie = function(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }
    
    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }    
$(document).ready(function(){

    //var baseUrl = "/Open-Catalogue/Retaining-Clip/Screwdrivers/KC9006-Cordless-Screwdriver/cart";
     var baseUrl = window.location.href;
    function getSecondPart(str,part) {
        return str.split(part)[1];
    }


    var pId = getSecondPart(baseUrl,'/p/');
    var cId = getSecondPart(baseUrl,'/c/');
    var cartId = baseUrl.includes('/cart');

    var prodPopShown = getCookie("prodPopShown");
    var coupPopShown = getCookie("coupPopShown");
    if(typeof pId !== "undefined" || typeof cId !== "undefined")
    {
        if(prodPopShown == "")
        {
            document.getElementById("modalPopup").innerHTML = "<div><button id=cancelButton style='float: right;border: 0;height: 15px;font-size: larger;color: #1c1d28;background: transparent;' aria-label='Close Account Info Modal Box'>&times;</button></div><div id=popupText style='padding: 14px 15px;'>We have some product suggestions for you. Click here to view.<div>";
            setTimeout(function(){
                if($('#myModalDai').is(':visible')) {
                    
                }
                else
                {
                    $("#modalPopup").show();
                }
               
                createCookie('prodPopShown',JSON.stringify({popUpShow : true}),0.000694444);

            }, 3000);
        }
        
    }
    else if(cartId)
    {
        document.getElementById("modalPopup").innerHTML = "<div><button id=cancelButton style='float: right;border: 0;height: 15px;font-size: larger;color: #1c1d28;background: transparent;' aria-label='Close Account Info Modal Box'>&times;</button></div><div id=popupText style='padding: 14px 15px;'>I have a discount coupon for you!<div>";
        if(coupPopShown == "")
        {
            setTimeout(function(){
                if($('#myModalDai').is(':visible')) {
                    
                }
                else
                {
                    $("#modalPopup").show();
                }
                createCookie('coupPopShown',JSON.stringify({popUpShow : true}),0.000694444);
            }, 3000);
        }
        
    }
    
   

    jQuery(function($) {
        $("#popupText").click(function(event){
            $("#modalPopup").hide();
            $("#myModalDai").toggle();
		var iframe = document.getElementById("web-iframe");
		var elmnt = iframe.contentWindow	   	
		elmnt.postMessage("scroll","*");
        });
    });

    jQuery(function($) {
        $("#cancelButton").click(function(event){
            $("#modalPopup").hide();
        });
    });

       
    function receiveMessage(event){
        if (event.data=="hideModal"){
            $('#myModalDai').hide();
            $('body').removeClass('modal-open');
        }
    }

   
    window.addEventListener("message", receiveMessage, false);

    $("#daichaticon").hide();
    $("#modalPopup").hide();

    setTimeout(function(){
        $("#daichaticon").show();
    }, 1000);

    setTimeout(function(){
        if($('#myModalDai').is(':visible')) {
            
        }
        else
        {
            
        }
       
    }, 3000);


    $('#myModalDai').hide();


    var i = 0;


    jQuery(function($) {
        $("#daichat-btn").click(function(event){
            chatIconClick();
        });
    });

    function chatIconClick() {
        jQuery(function ($) {
            $("#modalPopup").hide();
            $("#myModalDai").toggle();
		var iframe = document.getElementById("web-iframe");
		var elmnt = iframe.contentWindow	   	
		elmnt.postMessage("scroll","*");
        });
    }

    function chatclose(){
        jQuery(function($) {$("#myModalDai").hide();
        });
    }
});
