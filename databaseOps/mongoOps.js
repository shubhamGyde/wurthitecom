/**
 * Created by root on 7/23/15.
 */
var MongoClient = require('mongodb').MongoClient;
var opts = {auth:{authdb:'admin'},
user:'admin',
pass:'admin123'
};
var MongoObj = require('mongodb').ObjectID;
var dbConfig = {}
dbConfig.mongoHost =  "mongodb://localhost:27017"
// var guid = require('guid');
var mongoOps = {

    initializeMongoDb: function () {
        var dbLink =  + "/rexysDb";
        MongoClient.connect(dbLink, function (err, db) {
            if(!err && db) {
                var clientsColl = db.collection("clients");
                db.close();
            }
        });
    },

    generateClient : function (regparams,callback) {
        var dbLink = dbConfig.mongoHost + "/clientMaster";
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var clientsColl = db.collection("authorization");
                clientsColl.findOne({email: regparams.email}, function (err, document) {
                    if (!err && document) {
                        clientsColl.findOne({
                            email: regparams.email,
                            password: auth.pwdHash(regparams.password)
                        }, function (err, document) {
                            db.close();
                            if (!err && document)
                                callback(document.clientId, document.clientKey);
                            else
                                callback(null, null);
                        });
                    }

                    else {
                        regparams.clientId = Date.now().toString(); //As Rahul says - This wont work when 2 clients signup at same time. Hoping to resolve this :)
                        regparams.clientKey = guid.create().value;
                        regparams.password = auth.pwdHash(regparams.password);
                        regparams.timeStamp = Date.now().toString();
                        regparams.remainingDays = 28;
                        regparams.status = "active";
                        regparams.plan = "free";
                        regparams.lastUpdateTime = new Date().getTime().toString();
                        delete regparams.inputConfirmPassword;
                        delete regparams.register;
                        clientsColl.insertOne(regparams);
                        db.close();
                        callback(regparams.clientId, regparams.clientKey);
                    }
                });
            }
        });
    },

    fetchClient : function (regparams,callback)
    {
        var dbLink = dbConfig.mongoHost + "/clientMaster";
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var clientsColl = db.collection("authorization");
                clientsColl.findOne({
                    email: regparams.email,
                    password: auth.pwdHash(regparams.password)
                }, function (err, document) {
                    db.close();
                    if (!err && document)
                        callback(document.clientId, document.clientKey);
                    else
                        callback(null, null);
                });
            }
        });
    },

    getClient : function(clientId,callback)
    {
        var dbLink = dbConfig.mongoHost + "/rexysDb";
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var clientsColl = db.collection("clients");
                clientsColl.findOne({
                    clientId: clientId
                }, function (err, document) {
                    db.close();
                    if (!err && document)
                        callback(document);
                    else
                        callback(null);
                });
            }
        });
    },

    insertDocs: function(clientName, collectionName, jsonArray, callback) {
        var dbLink = dbConfig.mongoHost + "/" + clientName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collectionName);
                coll.insertMany(jsonArray, function(){
                    db.close();
                    callback(true);
                });
            }
        });
    },

    insertDoc: function (dbName, collectionName, json, callback) {
       // console.log("insert doc called");
        var dbLink = dbConfig.mongoHost + "/" + dbName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collectionName);
                coll.insert(json, function () {
                 //   console.log("insert doc executed");
                    db.close();
                  //  console.log("insert db close");
                    callback(true);
                });

            }
        });
    },

    updateDoc: function(dbName, collName, filter, json, callback) {
       // console.log("update doc called");
        var dbLink = dbConfig.mongoHost + "/" + dbName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collName);
                coll.updateOne(filter, json, {upsert:true}, function(){
                //    console.log("update doc executed");
                    db.close();
                 //   console.log("update db closed");
                    callback(true);
                });
            }
        });
    },

    removeAllDocuments: function(clientName, collectionName, callback){
        var dbLink = dbConfig.mongoHost + "/" + clientName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collectionName);
                coll.removeMany({}, function(){
                    db.close();
                    callback(true);
                });
            }
        });
    },
    storeUserData : function (clientName,userevents, callback) {
        var dbLink = dbConfig.mongoHost + "/" + clientName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection("tracker");
                userevents.timeStamp = new Date().getTime();
                coll.insertOne(userevents, function(){
                    //console.log("Tracker Document Added");
                    db.close();
                    callback(true);
                });
            }
        });
    },

    getDocuments: function (clientName, collName, callback) {
        var dbLink = dbConfig.mongoHost + "/" + clientName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collName);
                coll.find().toArray(function(err, resultArray) {
                    db.close();
                    callback(resultArray);
                });
            }
        });
    },

    getDocumentSelectedFields: function (clientName, collName, filter /* all docs = {} */, selectFields /* { fieldName : 1, notField : 0 } */, callback) {
        var dbLink = dbConfig.mongoHost + "/" + clientName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collName);
                coll.find(filter, selectFields).toArray(function(err, resultArray) {
                    db.close();
                    callback(resultArray);
                });
            }
        });
    },


    getDocument: function (clientName, collName, filter /* all docs = {} */, callback) {
      console.log("get doc called");
        var dbLink = dbConfig.mongoHost + "/" + clientName;
        MongoClient.connect(dbLink, function (err, db) {
              console.log('----->'+err);

            if (!err && db) {
                var coll = db.collection(collName);
                coll.find(filter).toArray(function (err, resultArray) {
                   // console.log("get doc executed");
                    db.close();
                   // console.log("get doc db closed");
                    callback(resultArray);
                });
            }
        });
    },

    getDistDocument: function (clientName, collName, columnName, filter, callback) {
        var dbLink = dbConfig.mongoHost + "/" + clientName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collName);
                coll.distinct(columnName,filter,(function(err, resultArray){
                   // assert.equal(null, err);
                    db.close();
                    callback(resultArray);
                }));
            }
        });
    },
    deleteDocument: function (clientName, collectionName, filter,callback) {
        var dbLink = dbConfig.mongoHost + "/" + clientName;
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collectionName);
                coll.removeOne(filter, function () {
                    db.close();
                    callback(true);
                });
            }
        });
    },
    updateField: function (dbName, collName, objId, updJsn, callback) {
        var dbLink = dbConfig.mongoHost + "/" + dbName;
        var obj_id = new MongoObj(objId);
        var filter = {'_id': obj_id};
        MongoClient.connect(dbLink, function (err, db) {
            if (!err && db) {
                var coll = db.collection(collName);
                coll.updateOne(filter, updJsn, function () {
                    db.close();
                    callback(true);
                });
            }
        });
    }
};

module.exports = mongoOps;

