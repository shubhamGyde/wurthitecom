var express = require('express');
var router = express.Router();
var app = express();
var dummyRouter = require('./dummyApi');
var bodyParser = require('body-parser');
var request = require('request');
var mongoOps = require('./databaseOps/mongoOps');
var config = require('./config/config');
var utils = require('./utils/utils');
var clientName = "wurthItEcom";
var clientDisplayName = "Client Name";
var dialogFlowAuthCode = "Bearer ae17f34c644c46e6aa6a0881c8a1de15";
var WebSocket = require('ws');
var fs = require('fs');
var http = require('http');
var https = require('https');

var slackUserToken = "";
var slackBotToken = "";
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

// const httpsLocalhost = require("https-localhost")
// const app = httpsLocalhost();

app.use(express.static(__dirname + '/'+clientName+'-support'));
app.use('/bld', express.static(__dirname + '/build'));
app.set('view engine', 'ejs');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));
var dbConfig = {
    mongoHost : "mongodb://localhost:27017"
};
var MongoClient = require('mongodb').MongoClient;

var privateKey  = fs.readFileSync('ssl/MyKey.key', 'utf8');
var certificate = fs.readFileSync('ssl/MyCertificate.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};
var httpsServer = https.createServer(credentials, app);
var httpsServerSocket = http.createServer(app);

const ws = new WebSocket.Server({server:httpsServerSocket});
var wsGlobal = [];
ws.on('connection', function connection(ws, req) {
ws.isAlive = true;
var userInfo ={};
userInfo.uid =  req.url.replace(/\//g,'');
userInfo.socket = ws

var i;
for(i=0;i<wsGlobal.length;i++)
{
     if(wsGlobal[i].uid == userInfo.uid)
     {
         wsGlobal[i].socket=ws;
         break;
     }
     }
     if(i==wsGlobal.length)
     {
         wsGlobal.push(userInfo);
         console.log("Socket created - " + userInfo.uid);
     }
 });


var chatUid = "";
router.get('/languageApi', function (req, res) {
    var jsn1 = req.query
    var jsn = JSON.stringify(jsn1);
    var authCode = dialogFlowAuthCode
    mongoOps.getDocuments(clientName,"slackTokens",function(result){
        if(result.length>0)
            {
                slackUserToken = result[0].clientAccessToken;
                slackBotToken = result[0].botAccessToken;
                botUserId = result[0].botUserId
            }
        });
        console.log("socket id "+jsn1.uid)
        chatUid = jsn1.chatUid;
        mongoOps.getDocument(clientName,"userMode",{userId : jsn1.uid}, function(rslt){
        console.log(rslt);
        if(rslt.length == 0 || (rslt[0].hasOwnProperty("userMode") && rslt[0].userMode == "auto"))
        {
            console.log("auto mode");
            if(jsn1.query=="32juh847g")
            {
                mongoOps.getDocument(clientName,"intentAnswers",{intentName : "Default Welcome Intent"},function(result){
                    res.send(result[0]);
                });
            }
            else
            {
                var options = { method: 'POST',
                url: "https://api.api.ai/api/query?v=20150910", 
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': authCode
                },
                body: jsn
                };
        
            jsn1.sender = "user";
            mongoOps.insertDoc(clientName,"chatLog",jsn1,function(){

            });

            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                    
                    var apiAiJson = JSON.parse(body);
                    var intentName = apiAiJson.result.metadata.intentName;
        
                    if(intentName == "custom-solution-question-yes" || intentName == "talk-to-expert"){
                        res.send({"actionName" : "calandlyAdd"});
                    }


                    else
                    executeIntent(body,jsn1,res,function(){

                    });
            });
            }    
        }
        else
        {
            console.log("manual mode");
            jsn1.userName = rslt[0].userName;
            var sessionId = jsn.uid;
            var manualJson = {
                mode : "manual"
            }
          
            slackPostMessage(jsn1);
            res.send(manualJson);
            
        }
    });
    
});

router.get('/getChatLog', function (req, res) {
    mongoOps.getDocument(clientName,"chatLog",{chatUid : req.query.chatUid},function(result){
        var prodCount = 0;
        var resLength = result.length-1;
        for(var i=resLength;i>=0;i--)
        {
            if(result[i].hasOwnProperty("products"))
            {
                if(prodCount < 3)
                {
                    prodCount = prodCount + 1;
                }
                else
                {
                    result.splice(i, 1);
                }
            }
        }
        res.send(result);
    });
}); 

function executeIntent(body,jsn1,res,callback)
{
    var cookieData;    
    if(jsn1.hasOwnProperty("cookieData") && jsn1.cookieData != "")
    {
        cookieData = JSON.parse(jsn1.cookieData);
    }
    
        var apiAiJson = JSON.parse(body);
        var intentName = apiAiJson.result.metadata.intentName;
        var resolvedQuery= apiAiJson.result.resolvedQuery;
        apiAiJson.sessionId = jsn1.sessionId;
        apiAiJson.timeStamp = Date.now();
        apiAiJson.sender = 'agent';

        if (apiAiJson.hasOwnProperty('result')) {
            if (intentName == "live-chat-handoff") {

                           var userDetails = {}
                //         userDetails.email = apiAiJson.result.parameters.email;
                           userDetails.givenName = "Anil Gupta";
                           userDetails.sessionId = jsn1.sessionId;

                           mongoOps.insertDoc(clientName,"userDetails",userDetails,function (rslt) {

                           mongoOps.getDocument(clientName,"chatLog",{sessionId : jsn1.sessionId},function (chatData) {
                                var emailData = {}
                                emailData.userDetails = userDetails;
                                var i = chatData.length;
                                emailData.chatLog = [];
                                emailData.chatLog.push(chatData[i-1]);
                                createTextMessage(emailData,function (messageText) {
                                  jsn1.query = "hi";    
                                  jsn1.messageText = messageText;                       
  
                                       slackConversationCreate(jsn1,userDetails,function(rslt){
                                            if(rslt)
                                            {
                                                
                                                    var respJsn = {};
                                                    respJsn.result = {};
                                                    respJsn.result.fulfillment = {}; 
                                                    respJsn.result.fulfillment.messages = [];
                                                    var msg = {
                                                                type: 4,
                                                                payload: {
                                                                data: ["Sure Anil. Someone from our team will get back to you soon."]
                                                                }
                                                            }   
                            
                                                    respJsn.result.fulfillment.messages.push(msg);
                                                    res.send(respJsn);
                                                
                                            }

                                            else
                                            {
                                                var respJsn = {};
                                                respJsn.result = {};
                                                respJsn.result.fulfillment = {}; 
                                                respJsn.result.fulfillment.messages = [];
                                                var msg = {
                                                            type: 4,
                                                            payload: {
                                                            data: ["Invalid name. Please do not enter any special characters. Click on following button to try again."],
                                                            buttons: ["Talk with senior executive"]
                                                            }
                                                        }   
                        
                                                respJsn.result.fulfillment.messages.push(msg);
                                                callback(respJsn);
                                            }
                                       });
                                    });
                                });
                           // });
                        });
                         
            }

        else if (intentName == "search-product") {

            if(apiAiJson.result.parameters.hasOwnProperty('searchTerm') && apiAiJson.result.parameters["searchTerm"] != '')
                {
                  var userDetails = {}
              

                    mongoOps.getDocument(clientName,"chatLog",{sessionId : jsn1.sessionId},function (chatData) {
                        var emailData = {}                
                        var searchTerm =  apiAiJson.result.parameters.searchTerm.replace(/ /g,'%20');
                        var options = { method: 'GET',
                                url: 'https://wurthbothybris.wurth-it.in/chatbotcommercewebservices/v1/powertools/products?query='+searchTerm,
                                headers: 
                                { 'postman-token': '2590c1b9-a4a2-4085-393d-5270d76e8a9e',
                                    'cache-control': 'no-cache',
                                    'content-type': 'application/json' },
                                body: 
                                {   
                                    query: '',
                                    lang: 'en',
                                    sessionId: '2423454545' },
                                    json: true 
                                };
                                
                                request(options, function (error, response, body) {
                                if (error) throw new Error(error);
                                    //console.log(body);
                                    var bodyJson = body;
                                    var searchUrl = "https://wurthbothybris.wurth-it.in/yb2bacceleratorstorefront/powertools/en/USD/search/?text="+searchTerm;

                                    if(bodyJson.hasOwnProperty('products'))
                                    {
                                        var prodLength = 5;
					                    if(prodLength > bodyJson.products.length)
					                    prodLength = bodyJson.products.length;
	    				
					

					                    for(var i=0;i<prodLength;i++)
                                            {
                                                bodyJson.products[i].imageUrl = bodyJson.products[i].images[1].url;
                                                bodyJson.products[i].desc = "Search for - "+ searchTerm;
                                                bodyJson.products[i].ctaText = "Know More"
                                                
                                            }
                                        
                                        bodyJson.data = ["To see more products, click<a href="+searchUrl+" target='_parent'> here</a>."];
                                        bodyJson.actionName = "productSearch";
                                        bodyJson.products = bodyJson.products.slice(0, prodLength);
                                        res.send(bodyJson);
                                        
                                        bodyJson.sender = "bot";
                                        bodyJson.timeStamp = Date.now();
                                        bodyJson.chatUid = chatUid;
                                        
                                        mongoOps.insertDoc(clientName,"chatLog",bodyJson,function(){

                                        });
                                    }
                                    else
                                    {
                                       
                                        var respJsn = {};
                                        respJsn.result = {};
                                        respJsn.result.fulfillment = {}; 
                                        respJsn.result.fulfillment.messages = [];
                                            var msg = {
                                                        type: 4,
                                                        payload: {
                                                                    data: ["This is what I've got for you.Click<a href="+searchUrl+" target='_parent'> here</a> to see the results."]
                                                                }
                                                      }   
                                    
                                        respJsn.result.fulfillment.messages.push(msg);
                                        
                                        respJsn.sender = "bot";
                                        respJsn.timeStamp = Date.now();
                                        respJsn.chatUid = chatUid;
                                        mongoOps.insertDoc(clientName,"chatLog",respJsn,function(){

                                        });
                                        
                                        res.send(respJsn);
                                    }
                                });         
                        });
                }

                else if(apiAiJson.result.fulfillment.hasOwnProperty("speech") && apiAiJson.result.fulfillment.speech != '')
                {
                            var respJsn = {};
                            respJsn.result = {};
                            respJsn.parameters = apiAiJson.result.parameters;
                            respJsn.result.fulfillment = {}; 
                            respJsn.result.fulfillment.messages = [];
                            var msg = {
                                    type: 4,
                                    payload: {
                                        data: [apiAiJson.result.fulfillment.speech],
                                    }
                                }
                            
                            respJsn.result.fulfillment.messages.push(msg);
                            res.send(respJsn);

                            var chatJson = {
                                sender : "bot",
                                timeStamp : Date.now(),
                                chatUid : chatUid,
                                payload: {
                                    data: [apiAiJson.result.fulfillment.speech],
                                }
                            }
                            
                            mongoOps.insertDoc(clientName,"chatLog",chatJson,function(){

                            });
                     }
                
                }

               else if(apiAiJson.result.fulfillment.hasOwnProperty("speech") && apiAiJson.result.fulfillment.speech != '')
                {
                            var respJsn = {};
                            respJsn.result = {};
                            respJsn.parameters = apiAiJson.result.parameters;
                            respJsn.result.fulfillment = {}; 
                            respJsn.result.fulfillment.messages = [];
                            var msg = {
                                    type: 4,
                                    payload: {
                                        data: [apiAiJson.result.fulfillment.speech],
                                    }
                                }
                            
                            respJsn.result.fulfillment.messages.push(msg);
                            res.send(respJsn);
                            
                            var chatJson = {
                                sender : "bot",
                                timeStamp : Date.now(),
                                chatUid : chatUid,
                                payload: {
                                    data: [apiAiJson.result.fulfillment.speech],
                                }
                            }
                            
                            mongoOps.insertDoc(clientName,"chatLog",chatJson,function(){

                            });
                }

                else if(intentName == "subscribe-newsletter")
                {
                        
                             mongoOps.insertDoc(clientName,"newsletterSubscribrs",{subscriberEmail : apiAiJson.result.parameters.email}, function(){
                                mongoOps.getDocument(clientName,"intentAnswers",{intentName : intentName},function (answerJson) {
                           
                                    if(answerJson.length > 0)
                                    {   
                                        answerJson[0].parameters = apiAiJson.result.parameters;
                                        res.send(answerJson[0]);
                                        
                                        apiAiJson[0].timeStamp = Date.now();
                                        apiAiJson[0].sender = "bot";
                                        apiAiJson[0].chatUid = chatUid;
                                        mongoOps.insertDoc(clientName,"chatLog",apiAiJson[0],function(){

                                        });
                                    }
                                });
                             });
                        
                    
                }

                else if(intentName == "track-order")
                {
                        if(apiAiJson.result.parameters.hasOwnProperty('orderNumber') && apiAiJson.result.parameters["orderNumber"] != '')
                        {
                             
                            var options = { method: 'POST',
                                url: 'https://wurthbothybris.wurth-it.in/authorizationserver/oauth/token?client_id=chatbot&client_secret=secret&grant_type=password&username=anil.gupta@pronto-hw.com&password=1234',
                                headers: 
                                { 'postman-token': '2590c1b9-a4a2-4085-393d-5270d76e8a9e',
                                    'cache-control': 'no-cache',
                                    'content-type': 'application/json' },
                                body: 
                                { query: '',
                                    lang: 'en',
                                    sessionId: '2423454545' },
                                json: true };

                                var productJson = {}; 
                                console.log(JSON.stringify(options));  
                                request(options, function (error, response, body) {
                                    var access_token = body.access_token;

                                    var options = { method: 'GET',
                                    url: 'https://wurthbothybris.wurth-it.in/chatbotcommercewebservices/v1/powertools/orders/'+apiAiJson.result.parameters.orderNumber+'?access_token='+access_token,
                                    headers: 
                                    { 'postman-token': '2590c1b9-a4a2-4085-393d-5270d76e8a9e',
                                        'cache-control': 'no-cache',
                                        'content-type': 'application/json' },
                                    body: 
                                    { query: '',
                                        lang: 'en',
                                        sessionId: '2423454545' },
                                    json: true };
    
                                    var productJson = {}; 
                                    console.log(JSON.stringify(options));  
                                    request(options, function (error, response, body) {
                                        if(body.hasOwnProperty('status'))
                                        {
                                        var respJsn = {};
                                        respJsn.result = {};
                                        respJsn.parameters = apiAiJson.result.parameters;
                                        respJsn.result.fulfillment = {}; 
                                        respJsn.result.fulfillment.messages = [];
                                        var orderDetails = 'https://wurthbothybris.wurth-it.in/yb2bacceleratorstorefront/powertools/en/USD/my-account/order/'+ apiAiJson.result.parameters.orderNumber;
                                        var msg = {
                                                type: 4,
                                                payload: {
                                                    data: ["Your order status is - " + body.status.code,"You can see the order details <a href="+orderDetails+" target='_parent'>here.</a> "],
                                                }
                                            }
                                        
                                        respJsn.result.fulfillment.messages.push(msg);
                                      

                                        respJsn.sender = "bot";
                                        respJsn.timeStamp = Date.now();
                                        respJsn.chatUid = chatUid;
                                        respJsn.payload = msg.payload;
                                        mongoOps.insertDoc(clientName,"chatLog",respJsn,function(){

                                        });
                                        
                                        res.send(respJsn);
                                    }
                                    else
                                    {
                                        var respJsn = {};
                                        respJsn.result = {};
                                        respJsn.parameters = apiAiJson.result.parameters;
                                        respJsn.result.fulfillment = {}; 
                                        respJsn.result.fulfillment.messages = [];
                                        var orderDetails = 'https://wurthbothybris.wurth-it.in/yb2bacceleratorstorefront/powertools/en/USD/my-account/order/'+ apiAiJson.result.parameters.orderNumber;
                                        var msg = {
                                                type: 4,
                                                payload: {
                                                    data: ["I could not find this order. Click on below button to try again."],
						                            buttons : ["Try track order again"]
                                                }
                                            }
                                        
                                        respJsn.result.fulfillment.messages.push(msg);
                                        res.send(respJsn);

                                        respJsn.sender = "bot";
                                        respJsn.timeStamp = Date.now();
                                        respJsn.chatUid = chatUid;
                                        mongoOps.insertDoc(clientName,"chatLog",respJsn,function(){

                                        });
                                        
                                        res.send(respJsn);

                                    }
                                    });                                    

                                });
                            
                        }
                    //}
                        else if(apiAiJson.result.fulfillment.hasOwnProperty("speech") && apiAiJson.result.fulfillment.speech != '')
                            {
                                var respJsn = {};
                                respJsn.result = {};
                                respJsn.parameters = apiAiJson.result.parameters;
                                respJsn.result.fulfillment = {}; 
                                respJsn.result.fulfillment.messages = [];
                                var msg = {
                                        type: 4,
                                        payload: {
                                            data: [apiAiJson.result.fulfillment.speech],
                                        }
                                    }
                                
                                respJsn.result.fulfillment.messages.push(msg);
                                res.send(respJsn);

                                var chatJson = {
                                    sender : "bot",
                                    timeStamp : Date.now(),
                                    chatUid : chatUid,
                                    payload: {
                                        data: [apiAiJson.result.fulfillment.speech],
                                    }
                                }
                                
                                mongoOps.insertDoc(clientName,"chatLog",chatJson,function(){
    
                                });
                         }
                }

                else {
                        
						mongoOps.getDocument(clientName,"intentAnswers",{intentName : intentName},function (answerJson) {
                           
							if(answerJson.length == 1)
                            {   
                                
                                answerJson[0].parameters = apiAiJson.result.parameters;
                                res.send(answerJson[0]);
                                
                                var chatJson = {
                                    sender : "bot",
                                    timeStamp : Date.now(),
                                    chatUid : chatUid,
                                    payload : answerJson[0].result.fulfillment.messages[0].payload
                                }
                                
                                mongoOps.insertDoc(clientName,"chatLog",chatJson,function(){

                                });
                            }
                            else if(answerJson.length == 0)
                            {
                                var respJsn = {};
                                respJsn.result = {};
                                respJsn.parameters = apiAiJson.result.parameters;
                                respJsn.result.fulfillment = {}; 
                                respJsn.result.fulfillment.messages = [];
                                var msg = {
                                        type: 4,
                                        payload: {
                                            data: ["Hello, I am Wurth Bot. How may I help you today? "],
                                            buttons: ["Search Products","Track Order","Talk to Customer Representative","Meet a Sales Representative"]
                                        }
                                    }
                                
                                respJsn.result.fulfillment.messages.push(msg);
								console.log(JSON.stringify(respJsn));
                                res.send(respJsn);

                                respJsn.sender = "bot";
                                respJsn.timeStamp = Date.now();
                                respJsn.chatUid = chatUid;
                                mongoOps.insertDoc(clientName,"chatLog",respJsn,function(){

                                });
                                        
                            }
                            else
                            {
                                for(i=0;i<answerJson.length;i++)
                                {
                                    if(answerJson[i].channel == jsn1.channel)
                                    {
                                        res.send(answerJson[i]);

                                        answerJson[i].sender = "bot";
                                        answerJson[i].timeStamp = Date.now();
                                        answerJson[i].chatUid = chatUid;
                                        mongoOps.insertDoc(clientName,"chatLog",answerJson[i],function(){

                                        });
         
                                        break;
                                    }
                                }
                            }
                        });
                }
            
        }
}


function slackPostMessage (jsn)
{
    var sessionId = jsn.uid;
    mongoOps.getDocument(clientName,"userMode",{userId : sessionId}, function(rslt){

        var options = { method: 'POST',
            url: 'https://slack.com/api/chat.postMessage',
            headers:
            {
                'cache-control': 'no-cache',
                'content-type': 'application/x-www-form-urlencoded' },
            form:
            {
                token: slackUserToken,
                channel: rslt[0].channel_name,
                text: jsn.query,
                as_user : "false",
                username : jsn.userName,
                icon_url : "http://lorempixel.com/48/48/"
            }
        };
        request(options, function (error, response, body) {

            if (error) throw new Error(error);
           // console.log(body);
        });
    //
    
});
}

router.post('/slackEventReceive', function (req, res) {
    var jsn = req.body;
    if(jsn.hasOwnProperty("challenge"))
    {
        res.send({"challenge":jsn.challenge});
    }
    else{
            if (jsn.event.type == "member_joined_channel" && slackUserToken !="")
                {
            
                }
            else if(jsn.event.type == "message" && !jsn.event.hasOwnProperty("subtype"))
            {
                
                mongoOps.getDocument(clientName,"channelMessage",{channelId : jsn.event.channel},function(result){
                    if(result.length >0 && result[0].assingedTo == "open")
                    {  
                        var userId = jsn.event.user;
                        var userName ="";
                        var options = { method: 'POST',
                url: 'https://slack.com/api/users.list',
                headers: 
                { 
                    'postman-token': '02011453-7662-1d72-6569-ebe0bda566bc',
                    'cache-control': 'no-cache',
                    'content-type': 'application/x-www-form-urlencoded' },
                form: { token: slackUserToken } };

                request(options, function (error, response, body) {
                
                if (error) throw new Error(error);
                var userJson = JSON.parse(body);
            

                for(var i=0;i<userJson.members.length;i++)
                    {
                        if(userJson.members[i].id == userId )
                        {
                            userName = userJson.members[i].real_name;
                            break;
                        }
                    }
        
                        var channelInfoText = "*" +userName + "* has joined - <https://discovery-ai.slack.com/archives/"+result[0].channelId+"|#" + result[0].channelName + ">"
                        options = { method: 'POST',
                            url: 'https://slack.com/api/chat.postMessage',
                            headers:
                            {
                                'cache-control': 'no-cache',
                                'content-type': 'application/x-www-form-urlencoded' },
                            form:
                            {
                                token: slackUserToken,
                                channel: "gyde_live_chat",
                                text: channelInfoText
                            }
                        };
                    request(options, function (error, response, body) {
                        if (error) throw new Error(error);
                        
                    });  
                }); 
                    result[0].assingedTo = userName;
                    mongoOps.deleteDocument(clientName,"channelMessage",{channelId : jsn.event.channel},function(){
                        delete result[0]["_id"];
                        mongoOps.insertDoc(clientName,"channelMessage",result[0],function(){

                        });
                    });
                }
                });
                
                
                res.sendStatus(200);
                slackReceive(jsn.event);  
            }    
            } 
});


router.get('/slackAuth', function (req, res) {
    console.log("in slack auth");
    var accessJson = {};
    var jsn = req.query
    var options = {
        method: 'GET',
        url: 'https://slack.com/api/oauth.access',
        qs: {
            client_id: '22859736448.476530967217',
            client_secret: '8a6dafe0baeb3143c3bc0bc9b7f4a743',
            code: jsn.code
        },
    }
    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        accessJsn = JSON.parse(body);
        accessJson = {
            type : "slackTokens",
            clientAccessToken : accessJsn.access_token,
            botAccessToken : accessJsn.bot.bot_access_token,
            botUserId : accessJsn.bot.bot_user_id,

        }
        
        mongoOps.getDocuments(clientName,"slackTokens",function(result){
            if(result.length>0)
            {
                mongoOps.deleteDocument(clientName,"slackTokens",{type:"slackTokens"},function(){
                    mongoOps.insertDoc(clientName,"slackTokens",accessJson,function(){
                        res.sendStatus(200);
                    });
                });
            }
            else
            {
                mongoOps.insertDoc(clientName,"slackTokens",accessJson,function(){
                    res.sendStatus(200);
                });
            }
            var options = { method: 'POST',
            url: 'https://slack.com/api/channels.create',
            headers:
            {
                'cache-control': 'no-cache',
                'content-type': 'application/x-www-form-urlencoded' },
            form:
            {
                token: accessJsn.access_token,
                name: "gyde_live_chat"
            }
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            var jsnBody = JSON.parse(body);



            if(jsnBody.hasOwnProperty("channel"))
            {
                var infoJson = {
                    id : jsnBody.channel.id,
                    name : "gyde_live_chat" 
                }
                mongoOps.getDocuments(clientName,"adminChannel",function(result){
                    if(result.length == 0)
                    {
                        mongoOps.insertDoc(clientName,"adminChannel",infoJson,function(){

                        });
                    }  
                    else
                    {
                        mongoOps.deleteDocument(clientName,"adminChannel",{name:"gyde_live_chat"},function(){
                            mongoOps.insertDoc(clientName,"adminChannel",infoJson,function(){

                            });
                        });
                    }  
                });
                
                
                
                var channelInfoText = "Gyde live chat has successfully installed. Please join the admin channel here.\n"
                channelInfoText += "Reply via Slack : https://discovery-ai.slack.com/archives/"+jsnBody.channel.id;
    
                    options = { method: 'POST',
                    url: 'https://slack.com/api/chat.postMessage',
                    headers:
                    {
                        'cache-control': 'no-cache',
                        'content-type': 'application/x-www-form-urlencoded' },
                    form:
                    {
                        token: accessJsn.access_token,
                        channel: "general",
                        text: channelInfoText
                    }
                };
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                
            });
        }
        });

    });    
});
});

function slackReceive(jsn) 
 {
    // res.sendStatus(200);
    console.log("channel info  " + JSON.stringify(jsn));
    if(!jsn.hasOwnProperty("subtype"))
    {
        mongoOps.getDocument(clientName,"channelMessage",{channelId : jsn.channel},function(result){    
        var channelName = ""
        if(result.length >0)
        var channelName = result[0].channelName.substr(0, result[0].channelName.indexOf('_')); 
        if(jsn.text == "auto")
    {
        var userJson = {
            userId : jsn.uid,
            userMode : "auto"
           }
        mongoOps.updateDoc(clientName,"userMode",{userId:jsn.channel},userJson,function(){
        });
    }

    else if(jsn.text == "close")
    {

       
        slackArchive(jsn.channel,channelName);
        mongoOps.deleteDocument(clientName,"userMode",{userId:jsn.channel},function(){
        });

    }

    else{
        
            for(var i=0;i<wsGlobal.length;i++)
            {
               if(wsGlobal[i].uid==channelName)
               {
                   jsn.text = jsn.text.replace(/es/g, "" );
                   jsn.sender = "live-agent";
                   jsn.uid = jsn.channel_name;
        
                   if (wsGlobal[i].socket.readyState === wsGlobal[i].socket.OPEN) {
                       wsGlobal[i].socket.send(jsn.text);
                   }
               }
            }
        }
        });
    }

}

function slackArchive(channelName,uid) {
    var options = { method: 'POST',
  url: 'https://slack.com/api/conversations.archive',
  headers: 
   { 'postman-token': 'c291e2f5-bd5f-bf68-fa42-9d9aa441c325',
     'cache-control': 'no-cache',
     'content-type': 'application/x-www-form-urlencoded' },
  form: 
   { token: slackUserToken,
     channel: channelName } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  for(var i=0;i<wsGlobal.length;i++)
  {
     if(wsGlobal[i].uid == uid)
     {
         wsGlobal[i].socket.close();
         wsGlobal.slice(i,1);
     }
  }
});
}

function slackConversationCreate(jsn,userDetails,callback)
{
    var adminChNm = ""
    var sessionId = jsn.uid;
    // mongoOps.getDocument(clientName,"userMode",{userId : sessionId}, function(rslt){
    var validChannelName = jsn.uid + "_" + userDetails.givenName;
    validChannelName = validChannelName.replace(/ /g,"_");
    validChannelName = validChannelName.toLowerCase();
    validChannelName = validChannelName.substring(0, 21);

    
    var options = { method: 'POST',
    url: 'https://slack.com/api/conversations.create',
    headers:
    {
        'cache-control': 'no-cache',
        'content-type': 'application/x-www-form-urlencoded' },
    form:
    {
        token: slackUserToken,
        name: validChannelName
    }
};
request(options, function (error, response, body) {
    if (error) throw new Error(error);
    var jsnBody = JSON.parse(body);
    if(jsnBody.hasOwnProperty("channel"))
    {
        
        var userJson = {
            userId : jsn.uid,
            userMode : "manual",
            userName : userDetails.givenName,
            channel_id : jsnBody.channel.id,
            channel_name : validChannelName
           }

       mongoOps.insertDoc(clientName,"userMode",userJson,function(){

       options = { method: 'POST',
        url: 'https://slack.com/api/conversations.leave',
        headers: 
         { 'postman-token': 'b512d2ac-dd48-6ecb-69ef-b2e96426446a',
           'cache-control': 'no-cache',
           'content-type': 'application/x-www-form-urlencoded' },
        form: 
         { token: slackUserToken,
           channel: jsnBody.channel.id
         } };
      
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
    
        var messageJson = {
            channelId : jsnBody.channel.id
        }

        var options = { method: 'GET',
        url: 'https://slack.com/api/channels.list',
        qs: 
        { token: slackUserToken,
        pretty: '1' },
         headers:   
        { 'postman-token': 'dce49108-a503-c4c4-581f-fe4e616bda54',
         'cache-control': 'no-cache' } };

        request(options, function (error, response, body) {
        if (error) throw new Error(error);
        channelInfo = JSON.parse(body)
        //console.log(body); 
        
        for(var i=0;i<channelInfo.channels.length;i++)
        {
            if(channelInfo.channels[i].id == jsnBody.channel.id)
            {
                messageJson.channelName = channelInfo.channels[i].name;
                messageJson.assingedTo = "open"
                
                var channelInfoText = "" 
                channelInfoText += "*Join New conversation* - <https://discovery-ai.slack.com/archives/"+jsnBody.channel.id+"|#" + channelInfo.channels[i].name + ">\n"
            
                options = { method: 'POST',
                        url: 'https://slack.com/api/chat.postMessage',
                        headers:
                        {
                            'cache-control': 'no-cache',
                            'content-type': 'application/x-www-form-urlencoded' },
                        form:
                        {
                            token: slackBotToken,
                            channel: "gyde_live_chat",
                            text: channelInfoText
                        }
                    };
                    request(options, function (error, response, body) {
                        var tsJson = JSON.parse(body)
                        if (error) throw new Error(error);
                        
                        mongoOps.getDocument(clientName,"channelMessage",{channelId : jsnBody.channel.id},function(){
                            
                        });
            
                        options = { method: 'POST',
                        url: 'https://slack.com/api/chat.postMessage',
                        headers:
                        {
                            'cache-control': 'no-cache',
                            'content-type': 'application/x-www-form-urlencoded' },
                        form:
                        {
                            token: slackBotToken,
                            channel: validChannelName ,
                            text: jsn.messageText
                        }
                    };
                    request(options, function (error, response, body) {
                        if (error) throw new Error(error);
                        callback(true);
                    });
                    });
                
                
   
                
                mongoOps.insertDoc(clientName,"channelMessage",messageJson,function(){
                
                }); 
                
            }
            if(channelInfo.channels[i].name == "gyde_live_chat")
            {
                adminChNm = channelInfo.channels[i].name;

                var infoJson = {
                    id : channelInfo.channels[i].id,
                    name : channelInfo.channels[i].name 
                }
                mongoOps.getDocuments(clientName,"adminChannel",function(result){
                    if(result.length == 0)
                    {
                        mongoOps.insertDoc(clientName,"adminChannel",infoJson,function(){

                        });
                    }    
                });
            }
        }
        });
        
    });
});
    }
    else
    {
        callback(false);
    }

    });    
}

function createTextMessage(userData, callback)
{
 var messageText = "There is a new live conversation requested by *"+userData.userDetails.givenName+"*! I am also attaching a conversation I had with him/her.\n";
 messageText +=   "\n\nASM link - https://wurthbothybris.wurth-it.in/yb2bacceleratorstorefront/?site=powertools&asm=true"

 for(var i = 0; i<userData.chatLog.length; i++)
    {
        
        if(userData.chatLog[i].sender == 'user')
        {
            if(userData.chatLog[i].hasOwnProperty('query'))
                if(userData.chatLog[i].query.toLowerCase() == "live assistance" || userData.chatLog[i].query.toLowerCase() == "asm")
                messageText +=  "\nUser requested for : Live Assistance";

                else
                messageText +=  "\nUser requested for : Live Chat";

        }
        else if(userData.chatLog[i].sender == 'agent')
        {
            if(userData.chatLog[i].result.fulfillment.messages.length > 0 && userData.chatLog[i].result.fulfillment.messages[0].hasOwnProperty("payload"))
            {
                var msg = userData.chatLog[i].result.fulfillment.messages[0].payload.data;
            
           
            messageText +=  "\nGyde: " 
            for(var j=0; j<msg.length; j++)
            {
               messageText +=  msg[j]; + "\n"
            }
        }
        }
    }
callback(messageText);

}

router.get('/hello', function (req, res) {
    res.send("hello");
});

router.get("/clientName", function (req, res) {
    res.render("wurthItEcom");});


function createHTML(rawData,callback){


    var html = '<html><head><meta name="viewport" content="width=device-width"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Simple Transactional Email</title><style>img{border:none;-ms-interpolation-mode:bicubic;max-width:100%}body{background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}table{border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%}table td{font-family:sans-serif;font-size:14px;vertical-align:top}.body{background-color:#f6f6f6;width:100%}.container{display:block;Margin:0 auto !important;max-width:580px;padding:10px;width:580px}.content{box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px}.main{background:#fff;border-radius:3px;width:100%}.wrapper{box-sizing:border-box;padding:20px}.content-block{padding-bottom:10px;padding-top:10px}.footer{clear:both;Margin-top:10px;text-align:center;width:100%}.footer td, .footer p, .footer span, .footer a{color:#999;font-size:12px;text-align:center}h1,h2,h3,h4{color:#000;font-family:sans-serif;font-weight:400;line-height:1.4;margin:0;Margin-bottom:30px}h1{font-size:35px;font-weight:300;text-align:center;text-transform:capitalize}p,ul,ol{font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px}p li, ul li, ol li{list-style-position:inside;margin-left:5px}a{color:#3498db;text-decoration:underline}.btn{box-sizing:border-box;width:100%}.btn>tbody>tr>td{padding-bottom:15px}.btn table{width:auto}.btn table td{background-color:#fff;border-radius:5px;text-align:center}.btn a{background-color:#fff;border:solid 1px #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px 25px;text-decoration:none;text-transform:capitalize}.btn-primary table td{background-color:#3498db}.btn-primary a{background-color:#3498db;border-color:#3498db;color:#fff}.last{margin-bottom:0}.first{margin-top:0}.align-center{text-align:center}.align-right{text-align:right}.align-left{text-align:left}.clear{clear:both}.mt0{margin-top:0}.mb0{margin-bottom:0}.preheader{color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0}.powered-by a{text-decoration:none}hr{border:0;border-bottom:1px solid #f6f6f6;Margin:20px 0}@media only screen and (max-width: 620px){table[class=body] h1{font-size:28px !important;margin-bottom:10px !important}table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a{font-size:16px !important}table[class=body] .wrapper, table[class=body] .article{padding:10px !important}table[class=body] .content{padding:0 !important}table[class=body] .container{padding:0 !important;width:100% !important}table[class=body] .main{border-left-width:0 !important;border-radius:0 !important;border-right-width:0 !important}table[class=body] .btn table{width:100% !important}table[class=body] .btn a{width:100% !important}table[class=body] .img-responsive{height:auto !important;max-width:100% !important;width:auto !important}}@media all{.ExternalClass{width:100%}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%}.apple-link a{color:inherit !important;font-family:inherit !important;font-size:inherit !important;font-weight:inherit !important;line-height:inherit !important;text-decoration:none !important}.btn-primary table td:hover{background-color:#34495e !important}.btn-primary a:hover{background-color:#34495e !important;border-color:#34495e !important}}</style></head><body class=""><table border="0" cellpadding="0" cellspacing="0" class="body"><tbody><tr><td>&nbsp;</td><td class="container"><div class="content"><table class="main"><tbody><tr><td class="wrapper"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td><p>Hi there,</p>';
    html += "<p><a href='mailto:" +rawData.userDetails.email + "'>" + rawData.userDetails.email + "</a> has requested to arrange a call.</p> <p>You may reach him / her on " +rawData.userDetails.phoneNumber + ".</p>";
    html += "<p><b>Below is the conversation he/she had with Ria</b></p>";

    for(var i = 0; i<rawData.chatLog.length; i++)
    {
        if(rawData.chatLog[i].sender == 'user')
        {
            if(rawData.chatLog[i].hasOwnProperty('query'))
                html += "<div style='position: relative; color: #2b313f; padding: 15px; border-radius: 3px; margin: 0px; min-height: 15px; border: 1px solid var(--mdc-theme-primary); margin-left: 15px; background-color: #efefef; font-size: 14px;'>User - " + rawData.chatLog[i].query + "</div>"
        }
        else if(rawData.chatLog[i].sender == 'agent')
        {
            var msg = rawData.chatLog[i].result.fulfillment.messages[0].payload.data;

            html += "<div style=' position: relative; color: white; padding: 15px; border-radius: 3px; margin: 0px; min-height: 15px;border: 1px solid var(--mdc-theme-accent); margin-left: 15px; background-color: #A5D175;margin-top: 5px; font-size: 14px;'>Ria - "
            for(var j=0; j<msg.length; j++)
            {
                html +=  msg[j] + "\n"
            }
            html += "</div>"
        }
    }
    html += "</td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table></body></html>";
    callback(html);
}

function createWeeklyDashboardHTML(rawData,unhandledRows,email,callback){
    var unhandled = '<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;"> <h1>Unhandled Conversations</h1> </td> </tr>';
    if(unhandledRows.length ==0) {
        unhandled += '<tr> <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;"> No Unhandled Conversations for the last week </td> </tr>';
    }
    else{
        for(var i=0;i<unhandledRows.length;i++){
            unhandled += '<tr> <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">'+ unhandledRows[i] +'</td> </tr>';
        }
    }
    unhandled += '</table>';
    var name = email.match(/^([^@]*)@/)[1];
    name = name.charAt(0).toUpperCase() + name.slice(1);
    var html = '<!doctype html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><!--[if gte mso 15]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml><![endif]--><meta charset="UTF-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="viewport" content="width=device-width, initial-scale=1"><title>*|MC:SUBJECT|*</title> <style type="text/css">p{margin:10px 0;padding:0;}table{border-collapse:collapse;}h1,h2,h3,h4,h5,h6{display:block;margin:0;padding:0;}img,a img{border:0;height:auto;outline:none;text-decoration:none;}body,#bodyTable,#bodyCell{height:100%;margin:0;padding:0;width:100%;}.mcnPreviewText{display:none !important;}#outlook a{padding:0;}img{-ms-interpolation-mode:bicubic;}table{mso-table-lspace:0pt;mso-table-rspace:0pt;}.ReadMsgBody{width:100%;}.ExternalClass{width:100%;}p,a,li,td,blockquote{mso-line-height-rule:exactly;}a[href^=tel],a[href^=sms]{color:inherit;cursor:default;text-decoration:none;}p,a,li,td,body,table,blockquote{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;}.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{line-height:100%;}a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}#bodyCell{padding:10px;}.templateContainer{max-width:600px !important;}a.mcnButton{display:block;}.mcnImage{vertical-align:bottom;}.mcnTextContent{word-break:break-word;}.mcnTextContent img{height:auto !important;}.mcnDividerBlock{table-layout:fixed !important;}/*@tab Page@section Background Style@tip Set the background color and top border for your email. You may want to choose colors that match your companys branding.*/body,#bodyTable{/*@editable*/background-color:#FAFAFA;}/*@tab Page@section Background Style@tip Set the background color and top border for your email. You may want to choose colors that match your companys branding.*/#bodyCell{/*@editable*/border-top:0;}/*@tab Page@section Email Border@tip Set the border for your email.*/.templateContainer{/*@editable*/border:0;}/*@tab Page@section Heading 1@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.@style heading 1*/h1{/*@editable*/color:#202020;/*@editable*/font-family:Helvetica;/*@editable*/font-size:26px;/*@editable*/font-style:normal;/*@editable*/font-weight:bold;/*@editable*/line-height:125%;/*@editable*/letter-spacing:normal;/*@editable*/text-align:left;}/*@tab Page@section Heading 2@tip Set the styling for all second-level headings in your emails.@style heading 2*/h2{/*@editable*/color:#202020;/*@editable*/font-family:Helvetica;/*@editable*/font-size:22px;/*@editable*/font-style:normal;/*@editable*/font-weight:bold;/*@editable*/line-height:125%;/*@editable*/letter-spacing:normal;/*@editable*/text-align:left;}/*@tab Page@section Heading 3@tip Set the styling for all third-level headings in your emails.@style heading 3*/h3{/*@editable*/color:#202020;/*@editable*/font-family:Helvetica;/*@editable*/font-size:20px;/*@editable*/font-style:normal;/*@editable*/font-weight:bold;/*@editable*/line-height:125%;/*@editable*/letter-spacing:normal;/*@editable*/text-align:left;}/*@tab Page@section Heading 4@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.@style heading 4*/h4{/*@editable*/color:#202020;/*@editable*/font-family:Helvetica;/*@editable*/font-size:18px;/*@editable*/font-style:normal;/*@editable*/font-weight:bold;/*@editable*/line-height:125%;/*@editable*/letter-spacing:normal;/*@editable*/text-align:left;}/*@tab Preheader@section Preheader Style@tip Set the background color and borders for your emails preheader area.*/#templatePreheader{/*@editable*/background-color:#FAFAFA;/*@editable*/background-image:none;/*@editable*/background-repeat:no-repeat;/*@editable*/background-position:center;/*@editable*/background-size:cover;/*@editable*/border-top:0;/*@editable*/border-bottom:0;/*@editable*/padding-top:9px;/*@editable*/padding-bottom:9px;}/*@tab Preheader@section Preheader Text@tip Set the styling for your emails preheader text. Choose a size and color that is easy to read.*/#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{/*@editable*/color:#656565;/*@editable*/font-family:Helvetica;/*@editable*/font-size:12px;/*@editable*/line-height:150%;/*@editable*/text-align:left;}/*@tab Preheader@section Preheader Link@tip Set the styling for your emails preheader links. Choose a color that helps them stand out from your text.*/#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{/*@editable*/color:#656565;/*@editable*/font-weight:normal;/*@editable*/text-decoration:underline;}/*@tab Header@section Header Style@tip Set the background color and borders for your emails header area.*/#templateHeader{/*@editable*/background-color:#FFFFFF;/*@editable*/background-image:none;/*@editable*/background-repeat:no-repeat;/*@editable*/background-position:center;/*@editable*/background-size:cover;/*@editable*/border-top:0;/*@editable*/border-bottom:0;/*@editable*/padding-top:9px;/*@editable*/padding-bottom:0;}/*@tab Header@section Header Text@tip Set the styling for your emails header text. Choose a size and color that is easy to read.*/#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{/*@editable*/color:#202020;/*@editable*/font-family:Helvetica;/*@editable*/font-size:16px;/*@editable*/line-height:150%;/*@editable*/text-align:left;}/*@tab Header@section Header Link@tip Set the styling for your emails header links. Choose a color that helps them stand out from your text.*/#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{/*@editable*/color:#2BAADF;/*@editable*/font-weight:normal;/*@editable*/text-decoration:underline;}/*@tab Body@section Body Style@tip Set the background color and borders for your emails body area.*/#templateBody{/*@editable*/background-color:#FFFFFF;/*@editable*/background-image:none;/*@editable*/background-repeat:no-repeat;/*@editable*/background-position:center;/*@editable*/background-size:cover;/*@editable*/border-top:0;/*@editable*/border-bottom:0;/*@editable*/padding-top:0;/*@editable*/padding-bottom:0;}/*@tab Body@section Body Text@tip Set the styling for your emails body text. Choose a size and color that is easy to read.*/#templateBody .mcnTextContent,#templateBody .mcnTextContent p{/*@editable*/color:#202020;/*@editable*/font-family:Helvetica;/*@editable*/font-size:16px;/*@editable*/line-height:150%;/*@editable*/text-align:left;}/*@tab Body@section Body Link@tip Set the styling for your emails body links. Choose a color that helps them stand out from your text.*/#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{/*@editable*/color:#2BAADF;/*@editable*/font-weight:normal;/*@editable*/text-decoration:underline;}/*@tab Columns@section Column Style@tip Set the background color and borders for your emails columns.*/#templateColumns{/*@editable*/background-color:#FFFFFF;/*@editable*/background-image:none;/*@editable*/background-repeat:no-repeat;/*@editable*/background-position:center;/*@editable*/background-size:cover;/*@editable*/border-top:0;/*@editable*/border-bottom:2px solid #EAEAEA;/*@editable*/padding-top:0;/*@editable*/padding-bottom:9px;}/*@tab Columns@section Column Text@tip Set the styling for your emails column text. Choose a size and color that is easy to read.*/#templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{/*@editable*/color:#202020;/*@editable*/font-family:Helvetica;/*@editable*/font-size:16px;/*@editable*/line-height:150%;/*@editable*/text-align:left;}/*@tab Columns@section Column Link@tip Set the styling for your emails column links. Choose a color that helps them stand out from your text.*/#templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{/*@editable*/color:#2BAADF;/*@editable*/font-weight:normal;/*@editable*/text-decoration:underline;}/*@tab Footer@section Footer Style@tip Set the background color and borders for your emails footer area.*/#templateFooter{/*@editable*/background-color:#FAFAFA;/*@editable*/background-image:none;/*@editable*/background-repeat:no-repeat;/*@editable*/background-position:center;/*@editable*/background-size:cover;/*@editable*/border-top:0;/*@editable*/border-bottom:0;/*@editable*/padding-top:9px;/*@editable*/padding-bottom:9px;}/*@tab Footer@section Footer Text@tip Set the styling for your emails footer text. Choose a size and color that is easy to read.*/#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{/*@editable*/color:#656565;/*@editable*/font-family:Helvetica;/*@editable*/font-size:12px;/*@editable*/line-height:150%;/*@editable*/text-align:center;}/*@tab Footer@section Footer Link@tip Set the styling for your emails footer links. Choose a color that helps them stand out from your text.*/#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{/*@editable*/color:#656565;/*@editable*/font-weight:normal;/*@editable*/text-decoration:underline;}@media only screen and (min-width:768px){.templateContainer{width:600px !important;}}@media only screen and (max-width: 480px){body,table,td,p,a,li,blockquote{-webkit-text-size-adjust:none !important;}}@media only screen and (max-width: 480px){body{width:100% !important;min-width:100% !important;}}@media only screen and (max-width: 480px){#bodyCell{padding-top:10px !important;}}@media only screen and (max-width: 480px){.columnWrapper{max-width:100% !important;width:100% !important;}}@media only screen and (max-width: 480px){.mcnImage{width:100% !important;}}@media only screen and (max-width: 480px){.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{max-width:100% !important;width:100% !important;}}@media only screen and (max-width: 480px){.mcnBoxedTextContentContainer{min-width:100% !important;}}@media only screen and (max-width: 480px){.mcnImageGroupContent{padding:9px !important;}}@media only screen and (max-width: 480px){.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{padding-top:9px !important;}}@media only screen and (max-width: 480px){.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{padding-top:18px !important;}}@media only screen and (max-width: 480px){.mcnImageCardBottomImageContent{padding-bottom:9px !important;}}@media only screen and (max-width: 480px){.mcnImageGroupBlockInner{padding-top:0 !important;padding-bottom:0 !important;}}@media only screen and (max-width: 480px){.mcnImageGroupBlockOuter{padding-top:9px !important;padding-bottom:9px !important;}}@media only screen and (max-width: 480px){.mcnTextContent,.mcnBoxedTextContentColumn{padding-right:18px !important;padding-left:18px !important;}}@media only screen and (max-width: 480px){.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{padding-right:18px !important;padding-bottom:0 !important;padding-left:18px !important;}}@media only screen and (max-width: 480px){.mcpreview-image-uploader{display:none !important;width:100% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Heading 1@tip Make the first-level headings larger in size for better readability on small screens.*/h1{/*@editable*/font-size:22px !important;/*@editable*/line-height:125% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Heading 2@tip Make the second-level headings larger in size for better readability on small screens.*/h2{/*@editable*/font-size:20px !important;/*@editable*/line-height:125% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Heading 3@tip Make the third-level headings larger in size for better readability on small screens.*/h3{/*@editable*/font-size:18px !important;/*@editable*/line-height:125% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Heading 4@tip Make the fourth-level headings larger in size for better readability on small screens.*/h4{/*@editable*/font-size:16px !important;/*@editable*/line-height:150% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Boxed Text@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.*/.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{/*@editable*/font-size:14px !important;/*@editable*/line-height:150% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Preheader Visibility@tip Set the visibility of the emails preheader on small screens. You can hide it to save space.*/#templatePreheader{/*@editable*/display:block !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Preheader Text@tip Make the preheader text larger in size for better readability on small screens.*/#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{/*@editable*/font-size:14px !important;/*@editable*/line-height:150% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Header Text@tip Make the header text larger in size for better readability on small screens.*/#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{/*@editable*/font-size:16px !important;/*@editable*/line-height:150% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Body Text@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.*/#templateBody .mcnTextContent,#templateBody .mcnTextContent p{/*@editable*/font-size:16px !important;/*@editable*/line-height:150% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Column Text@tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.*/#templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{/*@editable*/font-size:16px !important;/*@editable*/line-height:150% !important;}}@media only screen and (max-width: 480px){/*@tab Mobile Styles@section Footer Text@tip Make the footer content text larger in size for better readability on small screens.*/#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{/*@editable*/font-size:14px !important;/*@editable*/line-height:150% !important;}}</style></head> <body><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Weekly Performance of DiscoveryAI Support Chatbot</span> <center> <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable"> <tr> <td align="center" valign="top" id="bodyCell"><!--[if (gte mso 9)|(IE)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;"><tr><td align="center" valign="top" width="600" style="width:600px;"><![endif]--><table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer"><tr><td valign="top" id="templatePreheader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;"> <tbody class="mcnTextBlockOuter"> <tr> <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;"><!--[if mso]><table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;"><tr><![endif]--><!--[if mso]><td valign="top" width="600" style="width:600px;"><![endif]--><!--[if mso]></td><![endif]--><!--[if mso]></tr></table><![endif]--> </td></tr></tbody></table></td></tr><tr><td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;"> <tbody class="mcnImageBlockOuter"> <tr> <td valign="top" style="padding:9px" class="mcnImageBlockInner"> <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;"> <tbody><tr> <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;"> <img align="center" alt="" src="https://gallery.mailchimp.com/a0d66b9700102d9b1b593e6fb/images/c95a521f-3c9c-4934-9982-da8587e6db15.jpg" width="256" style="max-width:256px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage"> </td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr><td valign="top" id="templateBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;"> <tbody class="mcnTextBlockOuter"> <tr> <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;"><!--[if mso]><table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;"><tr><![endif]--><!--[if mso]><td valign="top" width="600" style="width:600px;"><![endif]--> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer"> <tbody><tr> <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;"> <h1>Hello '+ name +',&nbsp;</h1>Here is the support chatbot performance for the last week<br><br>&nbsp; </td></tr></tbody></table><!--[if mso]></td><![endif]--><!--[if mso]></tr></table><![endif]--> </td></tr></tbody></table></td></tr><tr><td valign="top" id="templateColumns"><!--[if (gte mso 9)|(IE)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;"><tr><td align="center" valign="top" width="200" style="width:200px;"><![endif]--><table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper"><tr><td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock"> <tbody class="mcnCaptionBlockOuter"> <tr> <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;"> <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="false"> <tbody><tr> <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;"> <img alt="" src="https://gallery.mailchimp.com/a0d66b9700102d9b1b593e6fb/images/6e05874d-fcaa-4578-86b5-42b7305dce0c.png" width="64" style="max-width:64px;" class="mcnImage"> </td></tr><tr> <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="164"> <div style="text-align: center;"><br><strong>Total Messages Processed</strong><br><br><span style="font-size:32px">'+rawData.totalMsgCnt+'</span><br>&nbsp;</div></td></tr></tbody></table> </td></tr></tbody></table></td></tr></table><!--[if (gte mso 9)|(IE)]></td><td align="center" valign="top" width="200" style="width:200px;"><![endif]--><table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper"><tr><td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock"> <tbody class="mcnCaptionBlockOuter"> <tr> <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;"> <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="false"> <tbody><tr> <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;"> <img alt="" src="https://gallery.mailchimp.com/a0d66b9700102d9b1b593e6fb/images/971efe2d-33f6-404f-8803-5826fb8b5727.png" width="64" style="max-width:64px;" class="mcnImage"> </td></tr><tr> <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="164"> <div style="text-align: center;"><br><strong>Total Unhandled Messages</strong><br>&nbsp;</div><div style="text-align: center;"><span style="font-size:32px">'+rawData.totalUnhandledMsgCnt+'</span><br>&nbsp;</div></td></tr></tbody></table> </td></tr></tbody></table></td></tr></table><!--[if (gte mso 9)|(IE)]></td><td align="center" valign="top" width="200" style="width:200px;"><![endif]--><table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper"><tr><td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock"> <tbody class="mcnCaptionBlockOuter"> <tr> <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;"> <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="false"> <tbody><tr> <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;"> <img alt="" src="https://gallery.mailchimp.com/a0d66b9700102d9b1b593e6fb/images/4cc0b8fd-052d-4bf5-9c90-d18dfdbc9581.png" width="64" style="max-width:64px;" class="mcnImage"> </td></tr><tr> <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="164"> <div style="text-align: center;"><br><strong>Total Leads Forwarded</strong><br>&nbsp;</div><div style="text-align: center;"><span style="font-size:32px">'+rawData.totalLeads+'</span><br>&nbsp;</div></td></tr></tbody></table> </td></tr></tbody></table></td></tr></table>' + unhandled + '<!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]--></td></tr><tr><td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;"> <tbody class="mcnDividerBlockOuter"> <tr> <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;"> <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EEEEEE;"> <tbody><tr> <td> <span></span> </td></tr></tbody></table><!-- <td class="mcnDividerBlockInner" style="padding: 18px;"> <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;"/>--> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;"> <tbody class="mcnTextBlockOuter"> <tr> <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;"><!--[if mso]><table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;"><tr><![endif]--><!--[if mso]><td valign="top" width="600" style="width:600px;"><![endif]--> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer"> <tbody><tr> <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;"> <em>Check detailed performance of your chatbot <a href="http://cs.discovery.ai:3000/#/login" target="_blank">here</a>.</em><br><br><strong>For any other query, please write us at contact@discovery.ai</strong> </td></tr></tbody></table><!--[if mso]></td><![endif]--><!--[if mso]></tr></table><![endif]--> </td></tr></tbody></table></td></tr></table><!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]--> </td></tr></table> </center> </body></html>';
    callback(html);
}


app.use('/rest', router);
app.use('/dummyRest', dummyRouter);

//httpsServer.listen(3003);
app.listen(3003);
httpsServerSocket.listen(3002);

